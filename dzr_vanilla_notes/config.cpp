class CfgPatches
{
	class dzr_vanilla_notes_scripts
	{
		requiredAddons[] = {};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_vanilla_notes
	{
		type = "mod";
		dir = "dzr_vanilla_notes";
		name = "dzr_vanilla_notes";
		author = "DayZ Russia";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_vanilla_notes/4_World"};
			};
		};
	};
};